/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniregistry.impl;

import java.util.HashMap;
import java.util.Map;
import lombok.ToString;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.DatumRegistry;
import net.angle.omniregistry.api.Registry;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public class BasicDatumRegistry<T extends Datum> extends BasicRegistry<T> implements DatumRegistry<T> {
    private final Map<String, T> entriesByName = new HashMap<>();
    
    protected BasicDatumRegistry(String name, int id, int registryID) {
        super(name, id, registryID);
    }
    
    public BasicDatumRegistry(String name, Registry<? extends Registry> registry) {
        super(name, registry);
    }
    
    @Override
    public <E extends T> E addEntry(E e) {
        super.addEntry(e);
        entriesByName.put(e.getName(), e);
        return e;
    }
    
    @Override
    public <E extends T> E getEntryByName(String name) {
        E entry = (E) entriesByName.get(name);
        if (entry == null)
            throw new NullPointerException("No such entry for name: " + name);
        return entry;
    }
}