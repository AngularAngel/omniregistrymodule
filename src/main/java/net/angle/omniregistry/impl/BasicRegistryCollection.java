/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniregistry.impl;

import com.samrj.devil.gl.ShaderProgram;
import java.util.ArrayList;
import java.util.List;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniregistry.api.TextureDefinitionRegistry;

/**
 *
 * @author angle
 */
public class BasicRegistryCollection extends BasicDatumRegistry<Registry<Datum>> implements RegistryCollection<Registry<Datum>>{
    private final List<TextureDefinitionRegistry> renderableRegistries = new ArrayList<>();
    
    public BasicRegistryCollection() {
        super("Registries", -1, -1);
    }

    @Override
    public TextureDefinitionRegistry addEntry(TextureDefinitionRegistry registry) {
        renderableRegistries.add(registry);
        return super.addEntry(registry);
    }
    
    @Override
    public void preparePalettes() {
        renderableRegistries.forEach((renderableRegistry) -> {
            renderableRegistry.preparePalette();
        });
    }
    
    @Override
    public void prepShader(ShaderProgram shader) {
        renderableRegistries.forEach((renderableRegistry) -> {
            renderableRegistry.prepShader(shader);
        });
    }
    
    @Override
    public void destroy() {
        renderableRegistries.forEach((renderableRegistry) -> {
            renderableRegistry.destroy();
        });
    }
}