package net.angle.omniregistry.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.SetRegistryDataDatagramAction;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicSetRegistryDataDatagramAction implements SetRegistryDataDatagramAction {
    private final @Getter int registryID;
    private final @Getter Datum[] data;
}
