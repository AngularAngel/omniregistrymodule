/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniregistry.impl;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.Renderable;

/**
 *
 * @author angle
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public abstract class AbstractRenderable extends AbstractDatum implements Renderable {
    private final @Getter int textureDefinitionID;
    private final boolean transparent, drawable;
    
    public AbstractRenderable(String name, Registry registry, int textureDefinitionID, boolean transparent, boolean drawable) {
        super(name, registry);
        this.textureDefinitionID = textureDefinitionID;
        this.transparent = transparent;
        this.drawable = drawable;
    }
    
    public AbstractRenderable(String name, int RegistryID, int id, int textureDefinitionID, boolean transparent, boolean drawable) {
        super(name, id, RegistryID);
        this.textureDefinitionID = textureDefinitionID;
        this.transparent = transparent;
        this.drawable = drawable;
    }
    
    @Override
    public boolean isTransparent() {
        return transparent;
    }
    
    @Override
    public boolean isDrawable() {
        return drawable;
    }
}