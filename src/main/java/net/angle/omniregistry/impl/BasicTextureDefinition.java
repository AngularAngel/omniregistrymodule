package net.angle.omniregistry.impl;

import com.samrj.devil.math.Vec4;
import lombok.Getter;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.TextureDefinition;

/**
 *
 * @author angle
 */
public class BasicTextureDefinition extends AbstractDatum implements TextureDefinition {
    private final @Getter Vec4[] palette;
    public BasicTextureDefinition(String name, Registry registry, Vec4[] palette) {
        super(name, registry);
        this.palette = palette;
    }
    
    public BasicTextureDefinition(String name, int RegistryID, int id, Vec4[] palette) {
        super(name, id, RegistryID);
        this.palette = palette;
    }
}