package net.angle.omniregistry.impl;

import java.util.Objects;
import lombok.Getter;
import lombok.Setter;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.DatumBuilder;
import net.angle.omniregistry.api.Registry;
import net.angle.omniserialization.impl.AbstractBuilder;

/**
 *
 * @author angle
 */
public abstract class AbstractDatumBuilder<T extends Datum> extends AbstractBuilder<T> implements DatumBuilder<T> {
    private @Getter @Setter String name;
    private @Getter Registry registry;
    private @Getter @Setter int id;
    private @Getter @Setter int registryID;

    public AbstractDatumBuilder(Class<T> objectClass) {
        super(objectClass);
    }
    
    @Override
    protected boolean isReady() {
        return Objects.nonNull(name) && (Objects.nonNull(registry) || (Objects.nonNull(id) && Objects.nonNull(registryID)));
    }

    @Override
    public void setRegistry(Registry<? super T> registry) {
        this.registry = registry;
        this.registryID = registry.getId();
        this.id = registry.getNextEntryID();
    }
}