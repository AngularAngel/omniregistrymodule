package net.angle.omniregistry.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.ToString;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryObject;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public class BasicRegistry<T extends RegistryObject> extends AbstractDatum implements Registry<T> {
    private final Map<Integer, T> entriesByID = new HashMap<>();
    
    protected BasicRegistry(String name, int id, int registryID) {
        super(name, id, registryID);
    }
    
    public BasicRegistry(String name, Registry<? extends Registry> registry) {
        super(name, registry);
    }
    
    @Override
    public int size() {
        return entriesByID.size();
    }
    
    @Override
    public <E extends T> E addEntry(E e) {
        int id = e.getId();
        if (entriesByID.containsKey(id)) {
            T t = entriesByID.get(e.getId());
            if (e.equals(t))
                return (E) t;
            else {
                throw new IllegalArgumentException("Registry " + getName() + " already contains element with ID " + id + ", " + getEntryById(id) + ", Cannot add " + e + "!");
            }
        }
        entriesByID.put(e.getId(), e);
        return e;
    }
    
    @Override
    public <E extends T> E getEntryById(int id) {
        return (E) entriesByID.get(id);
    }
    
    @Override
    public List<T> getAllEntries() {
        return Collections.unmodifiableList(List.copyOf(entriesByID.values()));
    }
}