package net.angle.omniregistry.impl;

import lombok.ToString;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omninetwork.impl.AbstractActionListDatagram;
import net.angle.omniregistry.api.RegistryCollection;

/**
 *
 * @author angle
 */
@ToString
public class BasicRegistryDatagram extends AbstractActionListDatagram<RegistryCollection> {
    public BasicRegistryDatagram(DatagramAction<RegistryCollection>[] actions, int priority) {
        super(actions, priority);
    }
    
    public BasicRegistryDatagram(DatagramAction<RegistryCollection> action, int priority) {
        super(new DatagramAction[] {action}, priority);
    }

    @Override
    public Class getTargetClass() {
        return RegistryCollection.class;
    }
}