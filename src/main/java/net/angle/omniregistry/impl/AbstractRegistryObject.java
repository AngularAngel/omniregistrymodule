package net.angle.omniregistry.impl;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.angle.omniregistry.api.RegistryObject;

/**
 *
 * @author angle
 */
@Getter
@EqualsAndHashCode
@ToString
public abstract class AbstractRegistryObject implements RegistryObject {
    private final int id;
    private final int registryID;
    
    protected AbstractRegistryObject(int id, int registryID) {
        this.id = id;
        this.registryID = registryID;
    }
}