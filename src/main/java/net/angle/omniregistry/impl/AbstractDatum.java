/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniregistry.impl;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.Registry;

/**
 *
 * @author angle
 */
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
public abstract class AbstractDatum extends AbstractRegistryObject implements Datum {
    private final String name;
    
    protected AbstractDatum(String name, int id, int registryID) {
        super(id, registryID);
        this.name = name;
    }
    
    public AbstractDatum(String name, Registry registry) {
        this(name, registry.getNextEntryID(), registry.getId());
    }
}