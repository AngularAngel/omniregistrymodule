package net.angle.omniregistry.impl;

import com.samrj.devil.math.Vec4;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.OmniRegistryModule;
import net.angle.omniserialization.impl.ReflectiveSerializer;

/**
 *
 * @author angle
 */
public class OmniRegistryModuleImpl extends BasicOmniModule implements OmniRegistryModule {

    public OmniRegistryModuleImpl() {
        super(OmniRegistryModule.class);
    }

    @Override
    public void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry) {
        try {
            serializerRegistry.registerObject(BasicTextureDefinition.class, new ReflectiveSerializer<>(BasicTextureDefinition.class.getConstructor(String.class, int.class, int.class, Vec4[].class), (t) -> {
                return new Object[]{t.getName(), t.getRegistryID(), t.getId(), t.getPalette()};
            })); 
            
            serializerRegistry.registerObject(Datum[].class, Datum[]::new);
            
            serializerRegistry.registerObject(BasicRegistryDatagram.class.getConstructor(DatagramAction[].class, int.class), (datagram) -> {
                return new Object[]{datagram.getActions(), datagram.getPriority()};
            });
            
            serializerRegistry.registerObject(BasicSetRegistryDataDatagramAction.class.getConstructor(int.class, Datum[].class), (datagramAction) -> {
                return new Object[]{datagramAction.getRegistryID(), datagramAction.getData()};
            });
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(OmniRegistryModuleImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}