package net.angle.omniregistry.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gl.Image;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.gl.TextureRectangle;
import com.samrj.devil.math.Util;
import com.samrj.devil.math.Vec4;
import lombok.ToString;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.TextureDefinition;
import static org.lwjgl.opengl.GL13C.GL_TEXTURE0;
import net.angle.omniregistry.api.TextureDefinitionRegistry;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public class BasicTextureDefinitionRegistry<T extends TextureDefinition> extends BasicDatumRegistry<T> implements TextureDefinitionRegistry<T> {
    private TextureRectangle palette;
    private final int texture;
    private final String uniformName;
    private int paletteLength = 0;
    
    public BasicTextureDefinitionRegistry(String name, Registry<Registry> registry, int texture, String uniformName) {
        super(name, registry);
        this.texture = texture;
        this.uniformName = uniformName;
    }

    @Override
    public <E extends T> E addEntry(E e) {
        super.addEntry(e);
        paletteLength = Math.max(e.getPalette().length , paletteLength);
        return e;
    }
    
    @Override
    public void preparePalette(Image.Shader shader) {
        destroy();
        palette = DGL.genTexRect();
        Image image = DGL.genImage(paletteLength, size(), 4, Util.PrimType.FLOAT);
        image.shade(shader);
        palette.image(image);
        DGL.delete(image);
    }
    
    @Override
    public void preparePalette() {
        preparePalette((int x, int y, int band) -> {
            if (size() <= y) return 0;
            
            Vec4[] renderPalette = getEntryById(y).getPalette();
            if (renderPalette.length <= x) return 0;
            if (band == 0)
                return renderPalette[x].x;
            if (band == 1)
                return renderPalette[x].y;
            if (band == 2)
                return renderPalette[x].z;
            if (band == 3)
                return renderPalette[x].w;
            else
                throw new IllegalArgumentException("Asked for Band: " + band);
        });
        
        palette.bind(texture);
    }
    
    @Override
    public void prepShader(ShaderProgram shader) {
        shader.uniform1i(uniformName, texture - GL_TEXTURE0);
    }
    
    @Override
    public void destroy() {
        if (palette != null) DGL.delete(palette);
    }
}