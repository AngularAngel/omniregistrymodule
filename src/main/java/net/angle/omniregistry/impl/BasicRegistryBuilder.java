package net.angle.omniregistry.impl;

import java.util.Objects;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryBuilder;

/**
 *
 * @author angle
 */
public class BasicRegistryBuilder<T extends Datum> extends AbstractDatumBuilder<BasicDatumRegistry<T>> implements RegistryBuilder<BasicDatumRegistry<T>, T> {
    public BasicRegistryBuilder(Class<BasicDatumRegistry<T>> cls) {
        super(cls);
    }
    
    @Override
    public BasicDatumRegistry<T> build() {
        checkBuildability();
        Registry registry = getRegistry();
        if (Objects.nonNull(registry))
            return build(new BasicDatumRegistry<>(getName(), registry));
        else
            return build(new BasicDatumRegistry<>(getName(), getId(), getRegistryID()));
    }
}