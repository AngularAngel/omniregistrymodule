package net.angle.omniregistry.impl;

import lombok.Getter;
import lombok.Setter;
import net.angle.omniregistry.api.Renderable;
import net.angle.omniregistry.api.RenderableBuilder;

/**
 *
 * @author angle
 */
public abstract class AbstractRenderableBuilder<T extends Renderable> extends AbstractDatumBuilder<T> implements RenderableBuilder<T> {
    
    private @Getter @Setter int textureDefinitionID = -1;
    private @Setter boolean transparent = false, drawable = true;

    public AbstractRenderableBuilder(Class<T> cls) {
        super(cls);
    }
    
    @Override
    public boolean isTransparent() {
        return transparent;
    }

    @Override
    public boolean isDrawable() {
        return drawable;
    }

    @Override
    protected boolean isReady() {
        return super.isReady() && textureDefinitionID >= 0;
    }
}