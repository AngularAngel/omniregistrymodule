module omni.registry {
    requires static lombok;
    requires devil.util;
    requires java.logging;
    requires omni.module;
    requires org.lwjgl.opengl;
    requires omni.serialization;
    requires omni.network;
    exports net.angle.omniregistry.api;
    exports net.angle.omniregistry.impl;
}