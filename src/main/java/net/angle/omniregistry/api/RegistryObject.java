package net.angle.omniregistry.api;

/**
 *
 * @author angle
 */
public interface RegistryObject {
    public int getId();
    public int getRegistryID();
}