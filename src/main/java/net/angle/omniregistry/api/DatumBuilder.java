package net.angle.omniregistry.api;

import net.angle.omniserialization.api.Builder;

/**
 *
 * @author angle
 */
public interface DatumBuilder<T extends Datum> extends Datum, Builder<T> {
    public void setName(String name);
    public void setRegistry(Registry<? super T> registry);
    public Registry<? super T> getRegistry();
}