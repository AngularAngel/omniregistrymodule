package net.angle.omniregistry.api;

/**
 *
 * @author angle
 */
public interface RenderableBuilder<T extends Renderable> extends Renderable, DatumBuilder<T> {
    public void setTextureDefinitionID(int textureDefinitionID);
    public void setTransparent(boolean transparent);
    public void setDrawable(boolean drawable);
}