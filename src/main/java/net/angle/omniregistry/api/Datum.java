package net.angle.omniregistry.api;

/**
 *
 * @author angle
 */
public interface Datum extends RegistryObject {
    public String getName();
}
