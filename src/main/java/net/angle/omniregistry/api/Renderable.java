package net.angle.omniregistry.api;

/**
 *
 * @author angle
 */
public interface Renderable extends Datum {
    public int getTextureDefinitionID();
    public boolean isTransparent();
    public boolean isDrawable();
}