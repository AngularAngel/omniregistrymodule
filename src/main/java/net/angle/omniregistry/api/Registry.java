package net.angle.omniregistry.api;

/**
 *
 * @author angle
 */
public interface Registry <T extends RegistryObject> extends Datum, Register<T> {
}