package net.angle.omniregistry.api;

import java.util.List;

/**
 *
 * @author angle
 */
public interface Register<T extends RegistryObject> {
    public <E extends T> E addEntry(E e);
    public default void addEntries(T[] entries) {
        for (T entry : entries)
            addEntry(entry);
    }
    
    public <E extends T> E getEntryById(int id);
    public List<T> getAllEntries();
    public int size();
    public default int getNextEntryID() {
        return size();
    }
}