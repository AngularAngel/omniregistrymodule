package net.angle.omniregistry.api;

import com.samrj.devil.gl.ShaderProgram;

/**
 *
 * @author angle
 */
public interface RegistryCollection<T extends Registry> extends DatumRegistry<T> {
    public TextureDefinitionRegistry addEntry(TextureDefinitionRegistry registry);
    public void preparePalettes();
    public void prepShader(ShaderProgram shader);
    public void destroy();
}