package net.angle.omniregistry.api;

import com.samrj.devil.math.Vec4;

/**
 *
 * @author angle
 */
public interface TextureDefinition extends Datum {
    public Vec4[] getPalette();
}