package net.angle.omniregistry.api;

import java.util.function.Supplier;

/**
 *
 * @author angle
 */
public interface RegistryBuilder<T extends Registry<E>, E extends Datum> extends DatumBuilder<T> {
    public void setId(int id);
}
