package net.angle.omniregistry.api;

import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omnimodule.api.OmniModule;

/**
 *
 * @author angle
 */
public interface OmniRegistryModule extends OmniModule {
    public void prepSerializerRegistry(ObjectSerializerRegistry registry);
}