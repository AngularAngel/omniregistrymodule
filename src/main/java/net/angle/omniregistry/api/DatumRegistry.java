package net.angle.omniregistry.api;

/**
 *
 * @author angle
 */
public interface DatumRegistry<T extends Datum> extends Registry<T> {
    public <E extends T> E getEntryByName(String name);
    
    public default T addEntry(DatumBuilder<T> builder) {
        builder.setRegistry(this);
        return addEntry(builder.build());
    }
}