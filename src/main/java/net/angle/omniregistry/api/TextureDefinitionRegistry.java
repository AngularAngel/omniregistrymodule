package net.angle.omniregistry.api;

import com.samrj.devil.gl.Image;
import com.samrj.devil.gl.ShaderProgram;

/**
 *
 * @author angle
 */
public interface TextureDefinitionRegistry<T extends TextureDefinition> extends DatumRegistry<T> {
    public void preparePalette(Image.Shader shader);
    public void preparePalette();
    public void prepShader(ShaderProgram shader);
    public void destroy();
}