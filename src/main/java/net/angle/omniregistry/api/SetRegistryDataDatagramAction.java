package net.angle.omniregistry.api;

import net.angle.omninetwork.api.DatagramAction;

/**
 *
 * @author angle
 */
public interface SetRegistryDataDatagramAction extends DatagramAction<RegistryCollection> {
    public int getRegistryID();
    public Datum[] getData();
    
    @Override
    public default void execute(RegistryCollection registries) {
        Registry registry = (Registry) registries.getEntryById(getRegistryID());
        registry.addEntries(getData());
        if (registry instanceof TextureDefinitionRegistry renderableRegistry)
            renderableRegistry.preparePalette();
    }
}