#version 140

uniform mat3 u_screen_matrix;
uniform vec2 u_fsq_size;
uniform vec2 u_max_dir;
uniform mat3 u_inv_dir_matrix;
uniform mat4[6] u_shadow_mats;

in vec2 in_pos;
in vec2 in_tex_coord;

out vec2 v_tex_coord;
out vec3 v_world_dir;
out vec3[6] v_shadow_world_dirs;

void main() {
    v_tex_coord = in_tex_coord;
    
    v_world_dir = u_inv_dir_matrix * vec3(((in_tex_coord * 2.0 / u_fsq_size) - 1.0) * u_max_dir, -1.0);
    
    for (int i = 0; i < 6; i++)
        v_shadow_world_dirs[i] = mat3(u_shadow_mats[i]) * v_world_dir;
    
    gl_Position = vec4(u_screen_matrix * vec3(in_pos, 0), 1.0);
}